//src/Travel2.js

import React from 'react';
import Travel from './travel';

const travels = [
    {
          destination: "Münster",
          country: "Germany",
          photo: "https://res.cloudinary.com/dzvkdhu9c/image/upload/c_scale,w_850/v1571242820/React%20travel%20app/30daysreplay-pr-marketing-L0Nr2kt6__k-unsplash_fiwyrk.jpg",
          distance: "2.400km"
    },
    {  
        
          destination: "Hawaii - Pipeline",
          country: "United States",
          photo: "https://res.cloudinary.com/dzvkdhu9c/image/upload/c_scale,w_850/v1571243534/React%20travel%20app/thomas-ashlock-7G5dkthFyxA-unsplash_grbbsl.jpg",
          distance: "12.600km"
    },     
    {
          destination: "Byron Bay",
          country: "Australia",
          photo: "https://res.cloudinary.com/dzvkdhu9c/image/upload/c_scale,w_850/v1571396442/React%20travel%20app/delphine-ducaruge-7DgLMKBJmdo-unsplash_s5kkmf.jpg",
          distance:"",
    },
    {
          destination: "Hamnøy",
          country: "Norway",
          photo: "https://res.cloudinary.com/dzvkdhu9c/image/upload/c_scale,w_850/v1571396330/React%20travel%20app/yuriy-garnaev-fcDnEf0TUV0-unsplash_l5gcgq.jpg",
          distance:"",
    },
    {
          destination: "Horseshoe Bend",
          country: "United States",
          photo: "https://res.cloudinary.com/dzvkdhu9c/image/upload/c_scale,w_850/v1571396442/React%20travel%20app/caroline-romano-FhbftiGioz0-unsplash_ywwgkq.jpg",
          distance:"",
    },
]


const Travels = () => (
    <div>
        {travels.map(travel => (
            <Travel key={travel.destination} newtravelobject={travel}/>
        ))}
    </div>
);







export default Travels;
