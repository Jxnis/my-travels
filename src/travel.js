// src/Quote.js
import React from "react";

const Travel = props => (
  <div>
    <img src={props.newtravelobject.photo} alt={props.newtravelobject.country} />
    <figcaption>
      <h2>{props.newtravelobject.destination}</h2>
      <cite>{props.newtravelobject.country}</cite>
    </figcaption>x
    <p>{props.newtravelobject.distance}</p>
  </div>
);

export default Travel;